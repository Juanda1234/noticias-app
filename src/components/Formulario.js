import React from 'react';
import styles from './Formulario.module.css';
import useSelect from '../hooks/useSelect';
import PropTypes from 'prop-types';

const Formulario = ({ guardarCategoria, guardarPais}) => {

    //utilizar custom hook

    

    const OPCIONES_CATEGORIA = [
        { value: 'general', label: 'General'},
        { value: 'business', label: 'Negocios'},
        { value: 'entertainment', label: 'Entretenimiento'},
        { value: 'health', label: 'Salud'},
        { value: 'science', label: 'Ciencia'},
        { value: 'sports', label: 'Deportes'},
        { value: 'technology', label: 'Tecnología'},
    ];

    const OPCIONES_PAISES = [
        { value: 'au', label: 'Australia'},
        { value: 'fr', label: 'Francia'},
        { value: 'de', label: 'Alemania'},
        { value: 'gb', label: 'Reino unido'},
        { value: 'se', label: 'Suecia'}
    ]

    const [ categoria, SelectNoticias ] = useSelect('Seleccione categoría', 'general', OPCIONES_CATEGORIA);

    const [ pais, SelectPais ] = useSelect('Seleccione país', 'au', OPCIONES_PAISES);

    //submit al form
    const buscarNoticia = e => {
        e.preventDefault();
        guardarCategoria(categoria)
        guardarPais(pais)
    }

    return ( 
        <div className={`${styles.buscador} row`}>
            <div className="col s12 m8 offset-m2">
                <form
                    onSubmit={buscarNoticia}
                >
                    <h2 className={styles.heading}>Encuantra noticias por categoría</h2>

                    <SelectNoticias />

                    <SelectPais />

                    <div className="input-field col s12">
                        <input
                            type="submit"
                            className={`${styles.btn_block} btn-large amber darken-2`}
                            value="Buscar"
                        />
                    </div>
                </form>
            </div>
        </div>
     );
}

Formulario.propTypes = {
    guardarCategoria: PropTypes.func.isRequired,
    guardarPais: PropTypes.func.isRequired
}
 
export default Formulario;