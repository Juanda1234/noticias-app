import React, { useState } from 'react';

const useSelect = (label, stateInicial, opciones) => {

    const [ state, actualizarState ] = useState(stateInicial);
    
    const Select = () => (
        <div className="row">
            <label>{label}</label>
            <select
                className="browser-default"
                value={state}
                onChange={e => actualizarState(e.target.value)}
            >
                <option value="">seleccione</option>
                {opciones.map(opcion => (
                    <option key={opcion.value} value={opcion.value}>{opcion.label}</option>
                ))}
            </select>
        </div>
    );

    return [ state, Select]

}
 
export default useSelect;