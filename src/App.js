import React, { Fragment, useState, useEffect } from 'react';
import Header from './components/Header';
import Formulario from './components/Formulario';
import ListadoNoticias from './components/ListadoNoticias';



function App() {

  const [ categoria, guardarCategoria ] = useState('');
  const [ pais, guardarPais ] = useState('');
  const [ noticias, guardarNoticias ] = useState([]);

  useEffect(() => {

    if(categoria === "" || pais === "") return null;

    const consultarAPI = async () => {
      const apiKey = process.env.REACT_APP_API_NEWS_KEY;
      const url = `https://gnews.io/api/v4/top-headlines?country=${pais}&topic=${categoria}&token=${apiKey}`;

      const respuesta = await fetch(url);
      const noticias = await respuesta.json();
      guardarNoticias(noticias.articles)
    }

    consultarAPI();
  }, [categoria, pais])

  return (
    <Fragment>
      <Header 
        titulo="Buscador de noticias"
      />

      <div className="container white">
        <Formulario
          guardarCategoria={guardarCategoria}
          guardarPais={guardarPais}
        />
        <ListadoNoticias
          noticias={noticias}
        />
      </div>
    </Fragment>
  );
}

export default App;
